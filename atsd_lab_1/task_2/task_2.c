﻿#define _CTR_SECURE_NO_WARNINGS_
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <math.h>
#include <string.h>
#include <time.h>

struct number
{
    signed short x;
};

union numberUn
{
    signed short x;
};

signed short getSign(signed short x)
{
    if (x == 0)
    {
        return 0;
    }
    return 1 | (x >> 15);
}
int main()
{
    struct number number1;
    union numberUn number2;
    signed short x;

    printf("Input 1st number:");
    scanf_s("%hd", &number1.x);
    printf("Input 2nd number:");
    scanf_s("%hd", &number2.x);
    printf("Input 3th number:");
    scanf_s("%hd", &x);

    printf("1st number: ");
    if (number1.x > 0)
    {
        printf("Number sign is +\n");
    }
    else if (number1.x < 0)
    {
        printf("Number sign is -\n");
    }
    else
    {
        printf("0\n");
    }

    printf("2nd number: ");
    if (number2.x > 0)
    {
        printf("Number sign is +\n");
    }
    else if (number2.x < 0)
    {
        printf("Number sign is -\n");
    }
    else
    {
        printf("0\n");
    }

    signed short sign = getSign(x);
    printf("3th number: ");
    if (sign > 0)
    {
        printf("Number sign is +\n");
    }
    else if (sign < 0)
    {
        printf("Number sign is -\n");
    }
    else
    {
        printf("0\n");
    }
    return 0;
}