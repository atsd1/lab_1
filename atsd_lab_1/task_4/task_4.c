﻿#define _CTR_SECURE_NO_WARNINGS_
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <math.h>


union myFloat {
	float d;
	unsigned char c[sizeof(float)];
};

void show(union myFloat mf) {
	printf("Bits: ");
	for (int i = sizeof(float) - 1; i >= 0; i--)
	{
		for (int j = 128; j; j >>= 1) {
			if (j & mf.c[i]) {
				printf("1");
			}
			else {
				printf("0");
			}
		}
	}

	printf("\n");

	for (int i = sizeof(float) - 1; i >= 0; i--)
	{
		printf("Byte to bits view %d: ", i);
		for (int j = 128; j; j >>= 1)
		{
			if (j & mf.c[i])
			{
				printf("1");
			}
			else
			{
				printf("0");
			}
		}
		printf("\n");
	}

	printf("Number sign: ");
	if (*(int*)&mf.d >> 31 >= 0)
	{
		printf("+\n");
	}
	else
	{
		printf("-\n");
	}

	printf("Mantissa: ");
	for (int i = sizeof(float) - 2; i >= 0; i--)
	{
		if (i == sizeof(float) - 2)
		{
			for (int j = 64; j; j >>= 1)
			{
				if (j & mf.c[i])
				{
					printf("1");
				}
				else
				{
					printf("0");
				}
			}
		}
		else
		{
			for (int j = 128; j; j >>= 1)
			{
				if (j & mf.c[i])
				{
					printf("1");
				}
				else
				{
					printf("0");
				}
			}

		}
	}
}


int main() {
	union myFloat num;

	printf("Input number: ");
	scanf_s("%f", &num.d);

	show(num);
	printf("\nSizeof = %d", sizeof(union myFloat));
}