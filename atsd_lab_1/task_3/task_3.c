#define _CTR_SECURE_NO_WARNINGS_
#include <stdio.h>
#include <stdlib.h>

int main() {
	signed char arr[] = {
		5 + 127,
		2 - 3,
		-120 - 34,
		(unsigned char)(-5),
		56 & 38,
		56 | 38
	};

	for (int i = 0; i < sizeof(arr)/sizeof(signed char); i++)
	{
		printf("%c: %d\n", 'A' + i, arr[i]);
	}
}