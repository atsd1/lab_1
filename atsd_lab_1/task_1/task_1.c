﻿#define _CTR_SECURE_NO_WARNINGS_
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

struct Date
{
	unsigned short nSecond : 6;
	unsigned short nMinutes : 6;
	unsigned short nHours : 5;
	unsigned short nWeekDay : 3;
	unsigned short nMonthDay : 5;
	unsigned short nMonth : 4;
	unsigned short nYear : 7;
	unsigned short nDayYear : 9;
};

int main()
{
	struct Date date;
	struct tm time;

	date.nSecond = 34;
	date.nMinutes = 41;
	date.nHours = 15;
	date.nWeekDay = 5;
	date.nMonthDay = 16;
	date.nMonth = 6;
	date.nYear = 22;
	date.nDayYear = 167;

	printf("struct Date = %d byte\n", (int)sizeof(date));
	printf("struct tm = %d byte\n\n", (int)sizeof(time));
	
	printf("Time: %d h. %d min. %d sec.\n", date.nHours, date.nMinutes, date.nSecond);
	printf("Date: %d.%d.%d\n", date.nMonthDay, date.nMonth, date.nYear);
	printf("Day of week: %d d.\n", date.nWeekDay);
	printf("Day of year: %d d.\n", date.nDayYear);

	float x = (int)pow(2, sizeof(float)*8 - 2);

	printf("%f: ", x);
	for (int i = sizeof(x) * 8 - 1; i >= 0; i--)
	{
		printf("%d", ((int)x >> i) & 1);
	}
}